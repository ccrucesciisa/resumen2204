<%-- 
    Document   : resultado
    Created on : 22-04-2021, 19:48:33
    Author     : ccruces
--%>

<%@page import="cl.model.Calculadora"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Calculadora cal = (Calculadora) request.getAttribute("Calculadora");
  
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>El resultado de la suma es: <%= cal.getSuma()%></h1>
    </body>
</html>
